# Satis Private Repositories

[![build status](https://gitlab.com/agencia110-public/satis-october/badges/master/build.svg)](https://gitlab.com/agencia110-public/satis-october/commits/master)

[https://agencia110-public.gitlab.io/satis-october](https://agencia110-public.gitlab.io/satis-october)

Add `composer.json`:

```
"repositories": [{
    "type": "composer",
    "url": "https://agencia110-public.gitlab.io/satis-october"
}],
```
